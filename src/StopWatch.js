import React, { Component } from 'react';



class StopWatch extends Component {
    constructor (component) {
        super(component)
        this.state = {
            seconds: 0,
            running: true,
            buttonLabel: "Stop"
        }
        this.handleSClick = this.handleSClick.bind(this);
        this.handleResetClick = this.handleResetClick.bind(this);
    }

    componentDidMount() {
        setInterval(() => {
            if (this.state.running) {
                this.setState({
                    seconds : this.state.seconds + 1,
                    buttonLabel : "Stop"
                });
            }   
        }, 1000);
     
    }
    
    handleSClick() {
        var runstate = (this.state.seconds === 0 ? true : false);
        this.setState({ 
            running: runstate
        });
    }

    handleResetClick() {
        this.setState({ 
            seconds: 0,
            buttonLabel: "Start"
        })
    }

    render() {
        return (
            <div>
                <p>Timer: {this.state.seconds}</p>
                <button onClick={this.handleSClick}>{this.state.buttonLabel}</button>
                <button onClick={this.handleResetClick}>Reset</button>
            </div>

        );
    }
}

export default StopWatch;